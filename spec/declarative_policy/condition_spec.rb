# frozen_string_literal: true

RSpec.describe DeclarativePolicy::Condition do
  subject(:condition) { described_class.new(name, opts, &compute_block) }

  let(:name) { 'condition_name' }
  let(:opts) { {} }
  let(:compute_block) { proc {} }

  describe 'deprecations' do
    let(:opts) { { scope: :normal } }

    it 'deprecates :normal scope' do
      notice = "[DEPRECATION] `:normal` is deprecated and will be removed in 2.0. " \
               "Please use new name `:user_and_subject`\n"

      expect { condition }.to output(notice).to_stderr
    end
  end
end
